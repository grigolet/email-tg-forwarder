FROM python:3.11

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY main.py main.py
COPY oauth2_flow.py oauth2_flow.py
COPY graph_client.py graph_client.py
COPY config.py config.py
USER 10001

CMD ["python3", "main.py"]
