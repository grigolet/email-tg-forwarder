"""Configuration definition."""
import ast
import os


ENV_DEV = "development"
ENV_PROD = "production"


class Config:
    """App configuration."""

    TOKEN_CACHE_FILE = os.getenv('TOKEN_CACHE_FILE', "token_cache.bin")
    
    AZURE_AUTHORITY = "https://login.microsoftonline.com/cern.ch"
    AZURE_CLIENT_ID = "543725ae-fd38-436a-a717-009b1a8137be"
    AZURE_SCOPE = [
        "https://graph.microsoft.com/.default",
    ]
    GRAPHAPI_BASEURL = "https://graph.microsoft.com/v1.0"


class DevelopmentConfig(Config):
    """Development configuration overrides."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "True"))


class ProductionConfig(Config):
    """Production configuration overrides."""

    ENV = os.getenv("ENV", ENV_PROD)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))


def load_config():
    """Load the configuration."""
    config_options = {ENV_DEV: DevelopmentConfig, ENV_PROD: ProductionConfig}
    environment = os.getenv("ENV", ENV_PROD)

    return config_options[environment]
