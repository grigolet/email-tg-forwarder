"""Process incoming emails."""
import requests
from config import Config
from oauth2_flow import Oauth2Flow


class GraphClient:
    """Process incoming emails."""

    def __init__(self, conf: Config, logger) -> None:
        """Initialize the Graph client."""
        self.conf = conf
        self.oauth = Oauth2Flow(conf)
        self.logger = logger

    def get_emails(self, nb_emails: int) -> None:
        """Return last nb_emails emails. nb_emails has to be in [1, 1000]"""
        nb_emails = min(max(nb_emails, 1), 1000)
        self.__access_token, self.__username = self.oauth.get_access_token()
        mails = self.__fetch_messages(folder_name="Inbox", limit=nb_emails)
        self.logger.info(f"{len(mails)} emails received.")
        return mails

    def __fetch_messages(self, folder_name: str, limit: int):
        """Return a list of messages in the specified folder."""
        # folder_id = self._find_folder_id_from_folder_path(folder_name)
        # If target folder is a well known folder, no need to get the ID.
        folder_id = folder_name
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/mailFolders/{folder_id}/messages"
        batch_size = limit
        messages: list
        # fields the query will retrieve, edit for more.
        params = {"$select": "id,sender,subject,uniqueBody"}
        params["$top"] = batch_size
        result = requests.get(
            request_url,
            headers={
                "Authorization": "Bearer " + self.__access_token,
                "Content-type": "application/json",
                "Prefer": 'outlook.body-content-type="html"',
            },
            params=params,
        )
        if result.status_code != 200:
            raise RuntimeError(f"Failed to fetch messages {result.text}")
        messages = result.json()["value"]
        # Loop if next page is present and not obtained message limit.
        while "@odata.nextLink" in result.json() and (batch_size - len(messages) > 0):
            result = requests.get(
                result.json()["@odata.nextLink"],
                headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            )
            if result.status_code != 200:
                raise RuntimeError(f"Failed to fetch messages {result.text}")
            messages.extend(result.json()["value"])
        return messages

    def create_folder(self, folder_name: str):
        """Create a folder."""
        sub_url = ""
        path_parts = folder_name.split("/")
        if len(path_parts) > 1:  # Folder is a subFolder
            parent_folder_id = None
            for folder in path_parts[:-1]:
                parent_folder_id = self._find_folder_id_with_parent(folder, parent_folder_id)
            sub_url = f"/{parent_folder_id}/childFolders"
            folder_name = path_parts[-1]

        request_body = {"displayName": folder_name}
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/mailFolders{sub_url}"
        resp = requests.post(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json=request_body,
        )
        if resp.status_code == 409:
            self.logger.debug("Folder %s already exists, skipping creation", folder_name)
        elif resp.status_code == 201:
            self.logger.debug("Created folder %s", folder_name)
        else:
            self.logger.warning("create_folder unknown response %s: %s", resp.status_code, resp.json())

    def __move_message(self, message_id: str, folder_name: str):
        """Move a message to another folder."""
        # folder_id = self._find_folder_id_from_folder_path(folder_name)
        # If target folder is a well known folder, no need to get the ID.
        # 'Archive' is one of them.
        folder_id = folder_name
        request_body = {"destinationId": folder_id}
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/messages/{message_id}/move"
        resp = requests.post(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json=request_body,
        )
        if resp.status_code != 201:
            raise RuntimeWarning(f"Failed to move message " f"{resp.status_code}: {resp.json()}")

    def __mark_message_read(self, message_id: str):
        """Mark a message as read."""
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/messages/{message_id}"
        resp = requests.patch(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json={"isRead": "true"},
        )
        if resp.status_code != 200:
            raise RuntimeWarning(f"Failed to mark message read" f"{resp.status_code}: {resp.json()}")
