from pydantic import BaseSettings, Field
from telegram import bot
import time
import logging
from pathlib import Path
from bs4 import BeautifulSoup
from config import load_config
from graph_client import GraphClient

class Settings(BaseSettings):
    TELEGRAM_BOT_TOKEN: str
    TELEGRAM_CHAT_ID: int
    LOGGING_LEVEL: str = "DEBUG"
    FETCH_BATCH: int = 1
    FETCH_PERIOD: int = 60 # in seconds

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


## NOTE: received emails are ordered from new to old and each email contains these fields
# {
#     "id": "*",
#     "subject": "*",
#     "sender": {
#         "emailAddress": {
#             "address": "*"
#         }
#     },
#     "uniqueBody": {
#         "content": "*"
#     }
# }


def run_main():
    settings = Settings()
    logging.basicConfig(level=settings.LOGGING_LEVEL, format='%(levelname)s:%(name)s:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(__name__)
    logger.setLevel(settings.LOGGING_LEVEL)
    bot_tg = bot.Bot(token=settings.TELEGRAM_BOT_TOKEN)
    email_config = load_config()
    try:
        logging.getLogger("msal").setLevel(logging.ERROR)
        logging.getLogger("urllib3").setLevel(logging.ERROR)
        logging.getLogger("telegram").setLevel(logging.ERROR)

        client = GraphClient(email_config, logger)
        mails = client.get_emails(nb_emails=1)
        while True:
            last_id = mails[0]['id']
            ## Fetch an increasing number of emails until the last_id is encountered
            nb_emails=settings.FETCH_BATCH
            mails = client.get_emails(nb_emails=nb_emails)
            while last_id not in [m['id'] for m in mails]:
                nb_emails *= 2
                mails = client.get_emails(nb_emails=nb_emails)
            logger.debug(f"Fetched {len(mails)} messages, last subject: {mails[0]['subject']}")
            ## Forward all new emails to Telegram
            for mail in mails:
                if mail['id'] != last_id:
                    logger.debug(f"Sending new email with subject: {mail['subject']}")
                    soup = BeautifulSoup(mail['uniqueBody']['content'], 'html.parser')
                    formatted_message = (
                        f"<b>Subject</b>: {mail['subject']}\n"
                        f"<b>Mailbox</b>: {mail['sender']['emailAddress']['address']}\n"
                        f"<b>Text</b>:\n{soup.body.text}"
                    )
                    bot_tg.send_message(
                        settings.TELEGRAM_CHAT_ID,
                        text=formatted_message,
                        parse_mode="HTML",
                    )
                else:
                    # The rest of the emails have already been received (mails are ordered from new to old)
                    break
            time.sleep(settings.FETCH_PERIOD)
    except Exception as e:
        logger.exception(e)
        time.sleep(settings.FETCH_PERIOD)
        run_main()

if __name__ == "__main__":
    run_main()
